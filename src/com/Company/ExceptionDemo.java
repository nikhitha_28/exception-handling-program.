package com.Company;

import java.util.*;

public class ExceptionDemo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double sruthiLuggageWeight = scan.nextDouble();
        double bhavyaLuggageWeight = scan.nextDouble();
        try {
            if (sruthiLuggageWeight > 15 || bhavyaLuggageWeight > 15) {
                System.out.println("The maximum luggage allowed is 15 Kg");
                throw new ArithmeticException();
            }
            else{
                System.out.println("Your luggage is with in the limit");
            }
        } catch (ArithmeticException amountToBePaid) {
            double sruthiExtraLuggage = sruthiLuggageWeight - 15;
            double bhavyaExtraLuggage = bhavyaLuggageWeight - 15;
            if (sruthiExtraLuggage > 0) {
                sruthiExtraLuggage *= 500;
                System.out.println("Try removing some of the luggage");
                System.out.println("Amount to be paid for extra luggage by shruthi is " + sruthiExtraLuggage);
            }
            if (bhavyaExtraLuggage > 0) {
                bhavyaExtraLuggage *= 500;
                System.out.println("Try removing some of the luggage");
                System.out.println("Amount to be paid for extra luggage by bhavya is " + bhavyaExtraLuggage);
            }
        }
    }
}
